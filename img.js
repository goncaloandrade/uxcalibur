const sharp = require("sharp"),
  fs = require("fs"),
  params = {
    src: "./content/img/post/",
    width: 1000,
    height: 2000
  },
  mode = process.env.MODE || "all";
console.log(`Running img processing. Mode is ${mode}`);

const stripExt = input => {
  return input
    .toLowerCase()
    .split(".")
    .filter(x => x.length);
};

const walkSync = function(dir = params.src, filelist) {
  let files = fs.readdirSync(dir);
  filelist = filelist || [];
  files.forEach(function(file) {
    if (fs.statSync(dir + file).isDirectory()) {
      filelist = walkSync(dir + file + "/", filelist);
    } else if (file.match(/(.min.)/) && (mode === "all" || mode === "delete")) {
      fs.unlink(`${dir}${file}`, err => {
        if (err) throw err;
        // console.log(`${dir}${file} was deleted`);
      });
    } else if (file.match(/(.png|.jpg|.jpeg)$/)) {
      const fileStrip = stripExt(file);
      const fileExists = fs.existsSync(
        `${dir}${fileStrip[0]}.min.${fileStrip[1]}`
      );
      if (fileExists) {
        console.log(`${dir}${file} already exists, skipping`);
      }
      if (
        mode === "add" ||
        ((mode === "all" || mode === "simple") && !fileExists)
      ) {
        filelist.push(`${dir}${file}`);
      }
    }
  });
  return filelist;
};

fs.readdir(params.src, function(err, files) {
  files = walkSync();
  files.forEach(function(file) {
    const img = stripExt(file);

    sharp(file)
      .resize(params.width, params.height, { fit: "inside" })
      .toFile(`.${img[0]}.min.${img[1]}`, (err, info) => {
        // console.log(file + " resized");
      })
      .toFile(`.${img[0]}.min.webp`, (err, info) => {
        console.log(err, img[0] + " to WEBP");
      });
  });
});
