---
layout: recommendation
title: Jorge Melo Braga
date: 2011-08-20 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601993338/Profile%20Pictures/Jorge_Melo_Braga.jpg
role: Events Marketing
job: Copidouro
linkedin: https://www.linkedin.com/in/jmelobraga/
---

I worked with Gonçalo for 1 year in Copidouro.
He had the role of change and dynamism to the company.
Faced with this challenge, Goncalo had the ability to respond to the task, leading, fostering a new interpretation of the role of business in the context of new media.
Clearly, Gonçalo, for his efforts, commitment and dynamism, a professional with a promising future.
