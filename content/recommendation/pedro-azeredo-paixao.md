---
layout: recommendation
title: Pedro Azeredo Paixão
date: 2013-05-20 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601993338/Profile%20Pictures/Pedro_Azeredo_Paixa%CC%83o.jpg
role: Software Engineer
job: SIMI
linkedin: https://www.linkedin.com/in/zepedropaixao/
---

Working with Gonçalo was a very good and profitable experience. As any project, the SIMI project had its moments of greater stress with hard deliverables to be accomplished in short periods of time. Gonçalo was always up for them and never failed showing extreme dedication and motivation levels. Adding to this, Gonçalo is a very easy person to deal with: he is very understanding, creative and implements (design and interface) in record speed times, which is really useful in a project with short deadlines. I hope we meet again one day in our professional lifes.
