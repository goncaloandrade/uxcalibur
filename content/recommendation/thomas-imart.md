---
layout: recommendation
title: Thomas Imart
date: 2016-05-20 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601993338/Profile%20Pictures/Thomas_Imart.jpg
role: Software Developer
job: Orbus
linkedin: https://www.linkedin.com/in/thomas-imart/
---

Goncalo is a talented UX designer who has been using his skills to create a coherence between our application and transformed a complex application to an intuitive one.

Beside his experience in UI/UX he is also an experienced UI developer who knows Javascript and AngularJS quite well. He is also able to adapt to the environment has he learnt in few weeks how to implement his design into a WPF application using XAML.

In addition Goncalo has a great personality and is a interesting person.
