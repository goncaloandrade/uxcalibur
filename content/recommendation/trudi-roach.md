---
layout: recommendation
title: "Trudi Roach"
date: 2018-05-30 10:45:06 +0000
job: "Aviva"
linkedin: "https://www.linkedin.com/in/trudicroach/"
image: ""
role: "Senior UX Researcher"
---

I’ve worked with Goncalo at Aviva on a couple of projects. Goncalo designed experiences to simplify complex financial problems, such as helping people understand how their pensions work or designing a tool to show much money they would have annually in retirement, which we tested in the lab to get feedback. I’ve also mentored Goncalo when he conducted user testing. He’s a quick learner, wrote excellent discussion guides, moderated extremely well, and produces reports with actionable findings. Goncalo will be an asset to any team, as he’s smart, a good teammate and fun to work with.
