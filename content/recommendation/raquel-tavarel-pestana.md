---
layout: recommendation
title: Raquel Tavares Pestana
date: 2012-01-20 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601993338/Profile%20Pictures/Raquel_Tavares_Pestana.jpg
role: Marketing Communications Specialist
job: GKM
linkedin: https://www.linkedin.com/in/raqueltavarespestana/
---

Goncalo is very comitted to finding solutions. His Master's in design, along with his coding skills make him stand out in a market increasingly more demanding. Along with his professional skills, his thoughtfulness and creativity are stand out traits of his personality. (translated)
