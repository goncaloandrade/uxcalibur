---
layout: recommendation
title: Tim Bouckley
date: 2020-06-02 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601993338/Profile%20Pictures/Tim_Bouckley.jpg
role: Service Design & Strategy Lead
job: Shell
linkedin: https://www.linkedin.com/in/timbouckley/
---

Goncalo is top-notch UX lead and a great self-starter. He joined our team at what was quite a chaotic time but he put the confusion to one side and started showing real value from day one. He mastered the complexity of the two products he was working across quickly and easily won the confidence of his teams and senior stakeholders. His extensive expertise in UX and Product design was a huge asset to our team.

As a person, Goncalo is easy going, highly professional and a pleasure to work alongside. He received nothing but positive feedback from his colleagues for his reliability and the quality of his work. I strongly recommend Goncalo, I’m only sorry we didn’t get to work together for longer.
