---
layout: portfolio
title: Publication Journey Mapping
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601930016/Publication%20Journey%20Mapping/publication-journey-mapping-featured.png
color: "#ee0000"
highlight: true
featured: true
keywords:
  - Web
  - Commercial
  - Workshop
  - Empathy Mapping
  - User Testing
date: 2019-12-16 10:45:06 +0000
---

## Making a customer-centric culture

When I was working for a household publication house, I had the opportunity to help [Stratis Valachis](linkedin.com/in/stratisv/), the UX Lead, with a pioneering journey mapping initiative.

This came about as the company culture at the time was very feature-focused and siloed. To address this issue, the plan was to encourage the company culture to move towards a more customer-centric mindset. To achieve this, we intended to introduce customer journey maps as a central part of our working practices. This would allow people to better see things from the customer's perspective and make our decisions based on customer outcomes.

##### How to change company culture

In order to get buy-in from several business layers, Stratis and I created a detailed customer journey map for one of the most important and recognizable brands the company had. We focused on the UK as a market so we could narrow down the focus of the experience we would look at. The resulting artefacts would then be the focus of a 3-day workshop which would involve representatives from the different markets the company had offices in including the US, Russia and Japan.

This workshop would introduce key stakeholders to design thinking through a series of exercises, as practice is the best way to learn the value of design thinking.

Finally, starting in 2020, we'd launch a larger initiative to create key journey maps for the rest of the company to use, focusing on different experiences, brands, and markets. With the priming done previously in the workshop, these maps should prove useful across the company.

## Building the map

In order to gather the information we needed for the map, we conducted 1h interviews with 11 participants, both readers and non-readers of the magazine. These were exploratory sessions, in which we focused on the individual experiences across platforms and brands.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930016/Publication%20Journey%20Mapping/publication-journey-mapping-1.jpeg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930016/Publication%20Journey%20Mapping/publication-journey-mapping-2.png)

From these in-depth interviews, we aggregated the data into [Learning Cards](https://uxdesign.cc/learning-cards-bf7fb204d2f6) (You can learn more about these in the article I've written about them on [Medium](https://uxdesign.cc/learning-cards-bf7fb204d2f6)). These were also backed by previous data (both quantitative and qualitative) already available from previous research.

We then constructed a story we could tell which would exemplify the typical user journey, as well as highlight the issues we had found in the lab.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930016/Publication%20Journey%20Mapping/publication-journey-mapping-5.svg)

The output was a complex experience map, which encompassed everything from when Maria (the actor we chose) wakes up in the morning and checks social media, when she reads a magazine on the train, when she sneakily checks the site at work, or when she unwinds at home in the evening with a glass of wine.

We included artefacts such as quotes, needs and a sentiment curve to make it easier to understand the entire experience from Maria's point of view, and where the best and worst parts of it are.

### Using the map at the workshop

A few days later we, together with the data analytics team, presented our work and our findings at the workshop. The initial reaction was a lot of questions from all over the room, mostly from people surprised by the findings, or who had never thought of those issues. This was our first success, as these are exactly the kinds of discussions we intended to trigger. By taking a high-level holistic view of the experience, we were able to find synergies between seemingly disparate features, which the silos within the company wouldn't be able to see.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930016/Publication%20Journey%20Mapping/publication-journey-mapping-3.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930016/Publication%20Journey%20Mapping/publication-journey-mapping-4.png)

For instance, we found that people would keep an article open in a tab on their phone for reading later at night, which in the analytics looks like a drop at first and then a separate visit. We also found that people were very interested in clothes worn by celebrities in pictures, but had no way of following up, so they'd drop off to go search for the items on Google.

However, we knew there were still some people in the room who didn't see the full value of this way of working. We had Developers and Senior Product Managers who were used to their way of doing things and were more worried about filling a backlog than shifting their way of thinking.

To demonstrate this, we took the participants through Empathy Mapping and Journey Mapping exercises. Going through the exercises themselves, with facilitation and guidance from Stratis and myself, helped the participants to think through the issues and focus on solutions rather than implementation of technology.

By the end of the workshop, each group was able to present innovative and customer centred propositions.
