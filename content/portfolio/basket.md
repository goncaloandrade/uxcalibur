---
layout: portfolio
title: Street Basket 3x3
date: 2006-01-27 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929564/Basket/basket-featured.png
color: "#cf2317"
keywords:
  - Graphic Design
  - Print
  - Poster
  - School
---
I got my degree at [ESAD University](https://goncaloandrade.com/client/esad/), the Escola Superior de Artes e Design (Higher Learning School of Arts and Design). Later I also got my MA (Master of Arts) there. Here I studied Arts and got a very hands-on education in design.

## Street basket tournament

A set of posters designed for a community street basket tournament organised by town hall.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929564/Basket/streetbasket-poster-01.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929564/Basket/streetbasket-poster-03.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929564/Basket/streetbasket-poster-02.png)