---
layout: portfolio
title: SIMI Digital Menus
date: 2013-09-06 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601930183/Simi/simi-featured.png
color: "#008FD5"
keywords:
  - App
  - Web
  - Commercial
  - Service Industry
---
## About the project

This app made eating out a premium experience, using an app to give clients all the choice and comfort. Clients wanted to browse my options in a quick and easy way and make their order hassle free. Business owners, on the other hand, wanted to show the users our products in an attractive way, and promote cross sales.

### What did I do?

I was responsible for the whole UX and UI of this project, as well as implementing the front-end together with another developer. This project was part of SIMI.

### So here’s the idea

Give the user a tablet with a mobile app where they can browse the restaurant’s offer, with professional high-resolution images. They can then place their order and the app itself communicates with the POS and takes care of everything.

For the business owner, give them control over every aspect of their menu and provide the ability to promote cross-sales, like a nice Bordeaux when this steak is ordered.

## Putting ideas to paper

Firstly, a solution needed to be found for displaying the products on a tablet screen. the primary orientation chosen was vertical, as an affordance to how people are used to consuming standard restaurant menus.

The 1st draft had the products scrolling horizontally under the restaurant information.

The product pages aimed to contain information about the product, such as descriptions as well as nutritional information and cross-sales. This idea tested poorly with the users dropping off the pages quickly and a low conversion rate.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-1.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-2.png)
![Product page with suggestions](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-3.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-4.png)
![Product page](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-5.png)
![Product page with cross-sales - alternate layout](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-6.png)

### Evolving the design

Through feedback the ideation, the design evolved into a simpler interface that allowed faster and easier access to the products. They became the star of the show.

Browsing switched to vertical and the whole screen became taken up by product tiles, discarding the idea of displaying multiple categories at once.

Instead, users would navigate to one category at a time. Though limited, this proved helpful in allowing the users to focus on one thing at a time.

Product pages now featured the high-resolution image much more prominently.

Icons with tooltips were used to compress the most relevant nutritional information, such as vegetarian and gluten-free statuses.

Cross-sales now appear in context, only once the user adds the product to the cart.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-7.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-8.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-9.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-10.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-11.png)

## Final product

The users can navigate a tiled interface with pictures and names of the products

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-12.png)

Large high resolution images make products attractive, and increase the conversion rate.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-13.png)

## So easy to manage

The business owner can access a web back-office to manage the offer anytime and anywhere. The interface uses the same tiled interface and a similar navigation, having users drill into specific categories to configure.

It was also made responsive and tested across mobile devices to facilitate access on the go.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-14.jpg)

SIMI’s responsive back-office

## Putting it to the test

We trialled it on a nice little family restaurant.

Both the clients and the business owner were very pleased with the results.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930182/Simi/simi-15.jpg)

The pilot test in an Italian restaurant