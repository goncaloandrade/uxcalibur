---
layout: portfolio
title: Marketing Platform
date: 2018-09-13 10:45:06 +0000
categories: null
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-featured.png
color: teal
---
## Improving how marketing plans are done

I was asked to improve the internal tool the marketing planners were using to do setup the marketing plans for their clients, distribute and manage the tasks to the network of freelancers

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-1.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-2.png)

## Pinpointing the problems

I ran some discovery workshops with the board members to understand the business needs and problems, and then with the marketing planners, so I could understand how they do their job, and what pain points they had.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-3.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-5.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-6.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-7.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-4.jpg)

It became apparent that there was a disconnect between the 2 groups. The way that the marketing planners were going about their work was not what their bosses thought.

I took note of all the workarounds and hacks they were using, and 3rd party tools, as well as why and what for. Then I could start to design a tool that would be useful for them.

## Finding solutions

The project then mostly consisted of streamlining the most important journeys and pruning [experience rot](https://articles.uie.com/experience_rot/).

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-8.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-9.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-10.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-11.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-12.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-13.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929903/Marketing%20Platform/marketing-platform-14.png)