---
layout: portfolio
title: Personal Shopping
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-featured.png
color: "#eac2b9"
featured: true
seoDescription: Helping personal stylists better serve their customers with a
  personal shopping experience, by designing a mobile appfocused arounf their
  needs
keywords:
  - Service Industry
  - App
  - Commercial
  - Service Design
  - User Testing
  - Journey Mapping
date: 2019-02-23 10:45:06 +0000
---
## Helping personal stylists better serve their customers

This household retail brand offers a new personal shopping service or experience now. In some of their shops, they were trialling a service where a customer can book an appointment with a personal stylist to help them put together outfits for every day or special occasions.

However, the systems made available to the stylists were clunky and not fit for purpose. As an example, they needed to go to the counter to check their bookings for the day, and whether any had changed or been cancelled because the scheduling system only worked on desktops set up in a very specific way.

This was not only annoying and unproductive but encouraged the stylists to find workarounds, or even not inputting the customer data into the system, and they don’t really see the value in it.

## Understanding the workflow

I started by mapping the personal shopping experience, with all the steps for both the customer and the stylist across all channels

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-1.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-3.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-2.jpg)

This proved particularly helpful as a discussion piece on the wall. I gathered the whole team multiple times around the experience map, which focused the discussion around user needs, as opposed to business requirements.

The solution which I started designing was a web app to be used mostly on mobile, which the stylists could carry around to help with their work.

Designing it to be a helpful assistant rather than bureaucratic busywork should help the stylists more easily see the value in using it and encourage them to use it. This, in turn, provides the company with valuable data about the customers.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-4.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-5.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-6.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-7.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-8.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-9.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-10.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-11.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-12.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-13.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930329/Stylist/stylist-14.jpg)