---
layout: portfolio
title: SIMI Mobile
date: 2013-09-06 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601930258/Simi%20Mobile/simi-mobile-featured.png
color: "#008FD5"
keywords:
  - App
  - Web
  - Commercial
  - Service Industry
---
## About the project

This was the companion app for [SIMI](/portfolio/simi), but from the client’s side.

They’d be able to browse options around them quickly and easily on their smartphones and to place orders hassle free anytime, from anywhere. And business owners would be able to reach further users and showcase the offering to more potential clients.

### What did I do?

I was responsible for the whole UX and UI of this project, as well as implementing the front-end together with another developer. This project was part of SIMI.

#### So here’s the idea

Let the users search for the restaurant and view the offer using SIMI. They can even place their order from their mobile app if they are in the restaurant.

The business owner can expose his or her products to more potential clients, and use their carefully crafted menu as a sales tool. They have control over every aspect of their menu and provide the ability to promote cross sales, like a nice Bordeaux to go with the stake you just ordered.

### Putting ideas to paper

The primary problem is how to help people navigate and see the products on a phone screen. The primary orientation chosen was vertical, as an affordance to how people are used to consuming standard restaurant menus.

The primary navigation was vertically scrolling tiles. However, due to the low power devices available in the market at the time, we needed to provide a fallback as buttons.

The initial drafts also included features that weren’t included in the final product, such as an off-canvas cart with expandable items.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930258/Simi%20Mobile/simi-mobile-1.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930258/Simi%20Mobile/simi-mobile-2.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930258/Simi%20Mobile/simi-mobile-3.jpg)

### Final Product

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930258/Simi%20Mobile/simi-mobile-5.png)

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930258/Simi%20Mobile/simi-mobile-6.jpg)

The app shows where the restaurants are, and highlights the ones with a SIMI menu.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930258/Simi%20Mobile/simi-mobile-11.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930258/Simi%20Mobile/simi-mobile-8.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930258/Simi%20Mobile/simi-mobile-9.jpg)

### So easy to manage

The business owner can access a web back-office to manage the offer anytime and anywhere

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601930258/Simi%20Mobile/simi-mobile-10.jpg)