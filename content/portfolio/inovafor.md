---
layout: portfolio
title: Inovafor
date: 2012-10-12 10:45:06 +0000
categories: null
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929872/Inovafor/inovafor-featured.png
color: "#353E7D"
keywords:
  - Graphic Design
  - Logo
---
### Professional training

This was a branding project for a company dealing with training adults in specific skills. The project came from the agency I was freelancing with at the time, Inovamais. They decided to contract me themselves to work on a new branch of their company.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929872/Inovafor/inovafor-3.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929872/Inovafor/inovafor-2.jpg)

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929872/Inovafor/inovafor-1.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929872/Inovafor/inovafor-4.jpg)