---
layout: portfolio
title: Customer Payments Portal
date: 2018-12-02 10:45:06 +0000
categories: null
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929969/Payment%20Portal/payment-portal-featured.png
color: "#c2185b"
---

I was brought in to help design the company’s first bespoke client-facing management system.

Up to that point, the customer experience was mostly comprised of passively receiving reports of transactions, and contacting the sales team on the phone.

The challenge was to understand the business logic and processes which happen in the background between the banks, payment terminal leasing companies and Paymentsense themselves, as well as for deciding how much of it the customer needed to be aware of.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929969/Payment%20Portal/payment-portal-1.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929969/Payment%20Portal/payment-portal-2.png)

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929969/Payment%20Portal/payment-portal-3.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929969/Payment%20Portal/payment-portal-4.png)
