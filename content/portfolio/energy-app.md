---
layout: portfolio
title: EDF Energy Glow
date: 2019-10-16 10:45:06 +0000
image: https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-featured.png
color: "#fa5021"
featured: true
keywords:
  - Energy
  - App
  - Commercial
  - Workshop
  - Empathy Mapping
  - User Testing
---
## Revitalising an old supplier with a new way of approaching energy

EDF Energy, as an old player in the energy supply market, is well established in its ways and practices.

RAP (Residential Accelerator Program) was established as an in-house disruptor team meant to work independently, sheltered from all the bulky old business and its inertia, to discover new ways of working and positioning in the market.

My colleagues and I worked through extensive discovery phases to tease out exactly what the customers think and feel, and how they relate to their energy provider.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-2.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-1.jpg)

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-3.jpg)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-4.jpg)

Through workshops and collaborative sessions, we got a good picture all that happens both in the several layers within the company and from the customer’s point of view.

## Taking a scientific approach

We then came up with hypotheses of what we thought would be true, such as *“Customers don’t provide meter reads because they are hard to do”* or *“Customers prefer to have their app secured with a fingerprint”*.

Then we planned a test through a discussion guide, making sure to be as unbiased as possible, and trying to put the participants in the situations we wanted to study and observing their behaviour, rather than asking them outright. Once we had that, we prepared whatever materials we’d need to run the sessions. This usually consisted of just a prototype of the app, but occasionally it would be necessary to draw up some emails or mockup some price comparison websites.

![Photo by [Adam Wilson](https://unsplash.com/@fourcolourblack?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText) on [Unsplash](https://unsplash.com/?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText)](https://images.unsplash.com/photo-1549732565-d673b928da7f?ixlib=rb-1.2.1&auto=format&fit=crop&w=1000&q=80)

The findings were then collated and processed into a report, to weed out any bias the researcher might have themselves (like overvaluing the findings from one very loud participant which would not be statistically significant).

Finally, we took the findings and altered the design to match what we found. Occasionally, if the findings weren’t clear enough, or there were still multiple solutions that fit the findings, we’d run 2 or 3 rounds of testing, to get to a level of certainty we’d be comfortable with.

![User flow diagram](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-5.png)

This allowed us to come up with a simplified and streamlined experience, that focused on customer needs rather than business or regulatory needs.

As an example, we focused on whether the payments were on track with our projections or needed to be adjusted, as opposed to showing a balance and expecting the customer to figure it out themselves. In fact, it turned out whenever they saw a positive balance they wanted to reduce their payments or take the money out, which would be bad for them since it’s meant to be positive to offset the cost of energy in winter.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-6.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-7.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-8.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-9.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-10.png)

Though it was not a redesign, you can see below the difference from the app made by the old business, versus the one we produced.

![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-11.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-12.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-13.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-14.png)
![](https://res.cloudinary.com/drzkcguqd/image/upload/v1601929724/Energy%20App/energy-15.png)