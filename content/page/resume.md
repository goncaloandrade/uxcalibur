---
title: "Resume"
date: 2019-12-01 10:45:06 +0000
---

## Work Experience

#### Senior User Experience Designer

![](https://media.licdn.com/dms/image/C560BAQEUXtrGCgAHtg/company-logo_100_100/0?e=1583366400&v=beta&t=C9FZB4HR_JR5Vio2kzAuo8EpKqIfv8sBP6ily06tgU4)[EDF Energy, London](https://www.edfenergy.com/) Apr 2019 – Oct 2019

Worked in the RAP team creating a new product to challenge the existing business and their way of working, disrupting from within.

This new service and product were app-only and heavily focused on empowering the customer.

> Lead the user research and conducted most of it, both moderated and unmoderated, in-person and remote.

> Designed radically different user journeys from the established in the business, backed with data and then tested Worked closely with Service Design, UI Design, and others in tight feedback loops for rapid iteration and progress

#### Senior User Experience Designer

![](https://media.licdn.com/dms/image/C4D0BAQEiQthZWB7alg/company-logo_100_100/0?e=1583366400&v=beta&t=X-c5Ux_jSJjQ1RT60XVMxFBQRPMzLT4aVsBA5gGIFyY) [John Lewis & Partners, London](http://johnlewis.com/) Jan 2019 – Apr 2019

Assumed ownership of the experimental pilot project for pioneering new user journeys throughout the upholstery (sofas and armchairs) section, and eventually inspiring the other teams through our work.

> Challenged most of the established UX patterns, always backed with research data

> Planned and conducted user research for rapid experimentation and iteration

> Liaised with other teams, including product, UX, commercial journeys, physical product design, in-store.

#### Lead User Experience Designer

![](https://media.licdn.com/dms/image/C4D0BAQHPLJKP56guTQ/company-logo_100_100/0?e=1583366400&v=beta&t=oUzdfwaZYB7wvcsbNUQtDJbjcr-LUg7YmozjqsE0Jpw) [Paymentsense, London](https://www.paymentsense.com/uk/) Oct 2018 – Dec 2018

Lead the project to create a client portal for the customers to view and manage their business’s card payments.

Understanding and simplifying the processes and information involved formed the majority of the work.

> Ran workshops to gather information on business processes and practices

> Conducted plenty of research to understand the user’s needs

> Made sure the design prioritised the user and discarded unecessary “business logic”

#### Lead User Experience Designer

![](https://media.licdn.com/dms/image/C560BAQEOLxfRkJl7vg/company-logo_100_100/0?e=1583366400&v=beta&t=cZhrDvZyuFBfnWFRIymdGEpNDaNUMk6sK1hTq0thxik) [Croud, London](https://croud.com/) Aug 2018 – Oct 2018

I worked on optimising the internal tool and client portal, as well as extending the new user
experience to the brand new client portal.

Complex and information-rich processes made this an interesting challenge.

I also worked on adapting and updating the branding guidelines to better fit a digital multimodal experience.

> Development of the foundational groundwork for UX principles within the company

> Planned and conducted user research after arguing its value in the design process

> Refined and extended the branding guidelines to better accommodate multimodal digital interfaces

#### Senior User Experience Designer

![](https://media.licdn.com/dms/image/C560BAQFy5j2jYnrG-Q/company-logo_100_100/0?e=1583366400&v=beta&t=DDj3o36aNipbCxDDk0EVApIy_xuo0FdYN95P5B6hh80) [Aviva, London](https://www.aviva.co.uk/) Feb 2017 – Jul 2018

I worked on optimising the user journeys across different sections of their UK website based on Adobe Experience Manager (AEM). After 6 months, conversion increased by 37.9% and sales journey completion by 26.7%.

I worked closely with DPOs to ensure the best products get built for the users.

> Proactive contribution to the long-term strategic development of UX practice including an understanding of knowledge, skills and consistent suite of processes.

> Lead SME through the full spectrum of UX services, workshops, tools, techniques & internal processes.

> Backed my work with proof by running research and testing, working closely with User Researchers and sometimes conducting all the testing myself.

> Iterative and collaborative design outputs: User journey maps, wireframes, prototypes, information architecture components (site structure, navigation, content grouping), functional specification documentation.

#### Senior User Experience Architect

![](https://media.licdn.com/dms/image/C4E0BAQEb-9MhHiQ-tg/company-logo_100_100/0?e=1583366400&v=beta&t=6hYgsElsETf1rcv9OchTgnMhQ_TnGBYzUppw5bCM6Ik) [Lloyds Bank, via Virtusa Polaris, London](https://www.lloydsbank.com/) Jul 2016 – Feb 2017

I worked in Lloyds Bank through a multinational consultancy facilitating, generating and executing great user experiences for our clients and their end users.
I took ownership of the projects acting as a coach, mentor and leader as well as an advocate for our clients’ needs with the business stakeholders.

> Full ownership of UX and Visual Design for the projects I worked on.

> Worked with BAs and SMEs to gain understnding of the technical financial products I was designing.

> Sat with the developers on a daily basis to ensure the experience got through to the final product, and to make some changes due to unforseen technical constraints.

#### Senior UX/UI Designer and Developer

![](https://media.licdn.com/dms/image/C4D0BAQG25RTOIc8EGg/company-logo_100_100/0?e=1583366400&v=beta&t=xq-VjrsComfMekFYaU9mAVo9PC7R9s977FN77Q4Ihlw) [Orbus Software, London](https://www.orbussoftware.com/) Feb 2014 – Jul 2016

A software company that specializes in Enterprise Architecture and Business Analysis.
I took ownership of the UX for 4 products, creating new experiences and interfaces that met the requirements, guaranteeing the overall quality and usability, produced high-fidelity mockups and specification documents, and managing and assisting in the coding of the products.

I also mentored a junior designer who was working for the sister company.

> Owned all UX and VD for 4 products as the sole designer.

> Worked with the developers in an agile Scrum setting in bi-weekly sprints.

> Created a complex Information Architecture and flexible modular design system, which as stood mostly unchanged since 2014, well after I left.

#### Design Consultant

![](https://inova.business/wp-content/uploads/2018/01/LOGO_header.png) [Inova+, Porto, Portugal](https://inova.business/en/) Mar 2011 – May 2016

This agency worked with international projects for the European Union, providing services from project management to funding advice and consulting.

I had the opportunity to work on branding several of their projects, from children’s programs to the modernisation of retirement homes.

I worked closely with the project managers, keeping a close professional relationship with them consistently throughout the project, to ensure meeting requirements as well as involving them in the design process.

#### UX/UI Designer and Developer

![](https://portalvhds726ybvjtpk8f4.blob.core.windows.net/video/2016/07/logo2-e1447099987965.png) [SIMI, Porto, Portugal](http://home.getsimi.com/) Aug 2012 – Feb 2014

The idea behind this startup was giving a restaurant’s client a tablet instead of a paper menu. This would enable a better sales platform. As a startup, this environment gave me the opportunity to get involved in all phases of the project, from discussing the requirements and backlog to writing user stories right down to the execution.

As a small team, each of us had many roles. I was fully responsible for designing the products (tablet, mobile and web apps) as well as coding a good deal of the interface.

#### Web Designer and Developer

GKM, Porto, Portugal Sep 2011 – Aug 2012

A service company providing a variety of digital services from social media marketing to website design and digital signage. I worked on a variety of projects, which allowed me to develop a good range of skills.

From assuming ownership of website design and development, mostly through WordPress, to producing digital assets for digital campaigns, as well as owning the user experience for a mobile app.

#### Graphic Designer

Copidouro, Porto, Portugal Oct 2010 – Aug 2011

Print services company, doing photocopies, larger print work, bookbinding, etc. I assumed a role as the sole Graphic Designer for the company and was tasked with the redesign of the brand.

This included redesigning their iconography and extended identity, graphic materials for the stores, social media presence and a new website. Working closely with experts I was involved in a lot of printing industry processes.

Learning and practising those production techniques has rendered my work more pragmatic and realist since I know how it will be executed.

#### Graphic Design and Production

WatchPlanet, Porto, Portugal Feb 2010 – Nov 2010

Reseller for watch brands such as Guess, Police, Ego, One, and others countrywide. I assisted in preparing campaigns’ graphical materials for final production and deployment in each store and their displays and windows, as well as some outdoors and billboards. Reseller for watch brands such as Guess, Police, Ego, One, and others countrywide.
