---
title: "About me"
date: 2019-09-16 10:45:06 +0000
categories:
type: "page"
layout: "simple"
image: "about-me/me.svg"
---

## Hi, I'm Gonçalo

I’m a User Experience Designer based in London with 10 years of experience, and I believe that Design is all about coming together to solve human problems.

I’ve been here for a few years now, but I’m originally from Portugal. This means my accent might change from British to Canadian or American at a moment’s notice, which is a lot of fun.

I enjoy reading comic books and playing Dungeons and Dragons, like the nerd I am. I am also almost always up for a pint and a chat down the pub.

I specialise in a number of different fields in my work, including:

| ![](about-me/journey-mapping.svg)Journey Mapping | ![](about-me/research.svg)User Research and Testing | ![](about-me/ia.svg)Information Architecture | ![](about-me/strategy.svg)Strategy | ![](about-me/critical-thinking.svg)Critical Thinking | ![](about-me/prototyping.svg)Prototyping | ![](about-me/workshops.svg)Workshops | ![](about-me/coding.svg)Coding |
| ------------------------------------------------ | --------------------------------------------------- | -------------------------------------------- | ---------------------------------- | ---------------------------------------------------- | ---------------------------------------- | ------------------------------------ | ------------------------------ |


#### My Process

##### 1. Discovery and learning

The first stage is all about defining and understanding the problem. Once we are asking the right questions, then we're ready to tackle them.

##### 2. Sketching and ideating

Next comes diverging ideas and working as a team to find the best way to solve the users' problems and pain points.

##### 3. Prototyping

Then we need to decide on a solution to take forward and put in front of users. We make sure we have a simulation of the product as close as possible to test with.

##### 4. Validation through user research

Putting in front of actual users and observing their behaviour is a crucial step. This is where the ideas come out of the silo and get to shine in the real world.

##### 5. Design polish and production

Based on the feedback and findings from the research the design is adjusted. It may be necessary to go have more research sessions, but once it's ready, it can start to be produced.

##### 6. Release, measure and iterate

After the product is released we get to see how it performs outside of a lab. We constantly measure its performance and adjust accordingly.

### [Get my CV](https://drive.google.com/open?id=1OzkbJgu5TTaFv54iCHe2MtDCBbsThigw)
